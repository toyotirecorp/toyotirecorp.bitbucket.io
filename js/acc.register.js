ACC.register = {
    _autoload: [
        "bindToTypeAheadInputBox",
    ],

    bindToTypeAheadInputBox: function ()
    {
    	var focusCount=0;
    	var dealers;
    	var dealerCode="";
    	var dealerUrl="register/dealers";
    	var positionUrl = "register/positions";
    	var mouseInCounter=1;
    	var keyPressed=true;
    	var pathname = window.location.pathname;
    	var phoneNumberCount=0;
		if(pathname.indexOf("newcustomersso") !== -1 || pathname.indexOf("newcustomer") !== -1){
			  dealerUrl="../register/dealers";
			  positionUrl="../register/positions";
		 }
		
		$.fn.selectRange = function(start, end) {
		    if(end === undefined) {
		        end = start;
		    }
		    return this.each(function() {
		        if('selectionStart' in this) {
		            this.selectionStart = start;
		            this.selectionEnd = end;
		        } else if(this.setSelectionRange) {
		            this.setSelectionRange(start, end);
		        } else if(this.createTextRange) {
		            var range = this.createTextRange();
		            range.collapse(true);
		            range.moveEnd('character', end);
		            range.moveStart('character', start);
		            range.select();
		        }
		    });
		};
		$("#phoneNumber").click(function(){
			if(phoneNumberCount==0){
				$("#phoneNumber").selectRange(1);
			}
			txtcount = txtcount+1;
		});
		
		$("#phoneNumber").mask('(999) 999-9999');
		$(".typeahead").autocomplete({
			source: function( request, response ) {
		        $.ajax({
		            dataType: "json",
		            type : 'Get',
		            url: dealerUrl,
		            data:
		            {
		                term: request.term
		            },
		            success: function(data) {
		            	var dealers = jQuery.parseJSON(data);
		            	response(dealers);
		            },
		            error: function(data) {
		            	console.log("Unable to load dealers");
		            }
		        });
		    },
		    minLength: 3,
            autoFocus: true,
            select: function (event, ui) {
            dealerCode = ui.item.value;
            }
        });
		
 
    	$(".typeahead").keyup(function(){
    		keyPressed=true;
    	});
    	
    	  $("#position").hover(function(){
    		  dealerCode = $(".typeahead").val();
    		  if(!dealerCode){
    			  $("#position").html('');
    			  $("#position").append($('<option ></option>').attr('value', '').text('SELECT POSITION'));
    			  $("select option:first").attr("disabled", "true");
    		  }
    		  if(keyPressed){
    		  $.ajax(
    				  {
    					  url: positionUrl, 
    					  data:'dealerCode='+dealerCode,
    					  beforeSend: function(){
    						  $("#position").html('');
    						  $("#position").append($('<option></option>').attr('value', '').text('SELECT POSITION'));
    						  $("select option:first").attr("disabled", "true");
      						},
    					  success: function(result){
    						  data = jQuery.parseJSON(result);
    						  keyPressed=false;
    						 $("#position").html('');
    						 $("#position").append($('<option></option>').attr('value', '').text('SELECT POSITION'));
    						 $("select option:first").attr("disabled", "true");
    						 $.each(data, function (key, entry) {
    							 $("#position").append($('<option></option>').attr('value', entry.label).text(entry.value));
    							  });
    					  }
    				  });
    		  }
    	  });
    	
    }
};