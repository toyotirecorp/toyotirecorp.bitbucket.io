ACC.warrantyToyo = {
  bindWillCallOptionCheck: function () {
    $(document).on("click", "#willCallCheckbox", function (e) {
      ACC.warrantyToyo.displayDriver(e);
    });
  },

  displayDriver: function (e) {
    var $willCallCheckbox = $("#willCallCheckbox");
    var checked = $willCallCheckbox.is(":checked");
    if (checked) {
      $(".driver-wrapper").show();
      $(".passanger-wrapper").hide();
    } else {
      $(".driver-wrapper").hide();
      $(".passanger-wrapper").show();
    }
  },

  refreshSubmitOrderButtonState: function () {
    var $termsAndConditionsCheckbox = $("#termsAndConditionsCheckbox");
    var checked = $termsAndConditionsCheckbox.is(":checked");
    var $placeOrder = $("#placeOrder");

    if (checked) {
      $placeOrder.removeClass("disabled");
    } else {
      $placeOrder.addClass("disabled");
    }
  },

  bindTermsAndConditionCheck: function () {
    $("#termsAndConditionsCheckbox").on("click", function () {
      ACC.warrantyToyo.refreshSubmitOrderButtonState();
    });
  },
};
$(document).ready(function () {
  with (ACC.warrantyToyo) {
    bindWillCallOptionCheck();
    bindTermsAndConditionCheck();
  }
});

$(".btn-danger").on("click", function () {
  if ($(this).hasClass("disabled")) {
    //  return false;
    $(this).removeClass("disabled");
  } else {
    $(this).addClass("disabled");
  }
});
