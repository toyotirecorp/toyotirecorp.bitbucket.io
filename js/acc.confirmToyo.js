ACC.confirmToyo = {
    _autoload: [

    ],

    show: function(params){
        if($('#confirmOverlay').length){
            // A confirm is already shown on the page:
            return false;
        }

        var buttonHTML = '';
        $.each(params.buttons,function(name,obj){

            // Generating the markup for the buttons:

            buttonHTML += '<a href="#" class="button btn-toyo '+obj['class']+'">'+name+'</a>';

            if(!obj.action){
                obj.action = function(){};
            }
        });

        var markup = [
            '<div id="confirmOverlay">',
            '<div id="confirmBox">',
            '<p>',params.message,'</p>',
            '<div id="confirmButtons">',
            buttonHTML,
            '</div></div></div>'
        ].join('');

        $(markup).hide().appendTo('body').fadeIn();

        var buttons = $('#confirmBox .button'),
            i = 0;

        $.each(params.buttons,function(name,obj){
            buttons.eq(i++).click(function(){

                // Calling the action attribute when a
                // click occurs, and hiding the confirm.
                obj.action(params.context);
                acc.confirmToyo.hide();
                return false;
            });
        });
    },

    showTemplate: function(params){
        if($('#confirmOverlay').length){
            // A confirm is already shown on the page:
            return false;
        }

        var markup = [
            '<div id="validOverlay">',
            '<div id="validBox">',
            $('<div/>').append(params.template).html(),
            '</div></div>'
        ].join('');

        $(markup).hide().appendTo('body').fadeIn();
    },

    hide: function() {
        $('#confirmOverlay').fadeOut(function () {
            $(this).remove();
        });
    }

}

$(document).ready(function () {

    with (ACC.bulkUploadToyo) {
        ACC.bulkUploadToyo.show();
        ACC.bulkUploadToyo.hide();
        ACC.bulkUploadToyo.showTemplate();
    }
});
