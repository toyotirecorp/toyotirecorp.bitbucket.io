
ACC.deliveryaddressToyo = {
    bindChangingAddressSelectionView: function () {
        function bindSwitch(button) {
            $(button).on("click", function (e) {
                $(".ship-to-address-selector").toggleClass("dropShipSelected");
                $("#js-d-s-k").toggleClass("wrapper-for-d-s-k-address");
                e.preventDefault();
                e.stopPropagation();
            })
        }

        var $switchToListBtn = $("#switch-to-list-selection");
        var $switchToDropShipBtn = $("#switch-to-drop-ship-selection");
        bindSwitch($switchToListBtn);
        bindSwitch($switchToDropShipBtn);
    },

    setShipToAddress: function (url, addressId) {
        $("#spannerContainer").empty();
        $("#spannerContainer").append(ACC.cartToyo.spinner);
        ACC.colorbox.open('Updating address...',{
            href: "#spannerContainer",
            inline: true,
            width:"525px",
            closeButton: false,
            onComplete: function ()
            {
                $(this).colorbox.resize();
                 $.ajax({
                    url: url,
                    data: {deliveryAddressId: addressId, dropShipSelected: false},
                    type: "POST",
                    success: function(data, textStatus, xhr) {
                        ACC.cartToyo.reloadCartPage();
                    },
                    error: function(xhr, textStatus, error) {
                        alert("Failed to update address.");
                    }
                });
            }
        });
    },
    
    setDefaultShipToAddress: function (url, defaultAddressId) {
                 $.ajax({
                    url: '/en/checkout/multi/summary/updateDeliveryAddressOnLoad',
                    data: {deliveryAddressId: defaultAddressId, dropShipSelected: false},
                    type: "POST",
                    success: function(data, textStatus, xhr) {
                        $(".dName").html(data.firstName);
                        $(".dLine1").html(data.line1);
                        $(".dLine2").html(data.town+", "+data.region.isocodeShort+", "+data.postalCode);
                        $(".dPhone").html(data.phone);
                        $(".dBranch").html(data.branch);
                    },
                    error: function(xhr, textStatus, error) {
                        alert("Failed to update address.");
                    }
                });
    },

    bindSelectionFromList: function () {
    	var url = $("#changeShipToUrl").val();
    	if($(".addr-list-item").find("option").length === 1){
    		$(".selectedAddress").hide();
    		$(".defaultAddress").show();
    		$(".selectedAddresslabel").show();
    		$("#shipToForm").css({position:'relative'});
			$("#shipToForm").css({display:'inline'});
    		var defaultAddressId =  $(".addr-list-item").val();
    		ACC.deliveryaddressToyo.setDefaultShipToAddress(url, defaultAddressId);
    	}else{
    		$(".defaultAddress").hide();
    		$(".selectedAddress").show();
    		$("#shipToForm").css({position:'relative'});
			$("#shipToForm").css({display:'inline'});
    		var $predefinedAddressListItems = $(".addr-list-item");
    		$predefinedAddressListItems.on("change", function () {
            var addressId = $(this).val();
            ACC.deliveryaddressToyo.setShipToAddress(url, addressId);
        });
    	}
    },

    bindSaveDropShipAddressButton: function () {
        var $dropShipAddressContent = $("#dropShipAddressContent");
        $dropShipAddressContent.on("click", "#saveDropShipAddressBtn", function (e) {
            e.stopPropagation();
            e.preventDefault();

            var promise = $.ajax({
                type: "POST",
                url: $("#saveDropShipAddressForm").attr("action"),
                data: $("#saveDropShipAddressForm").serialize() // serializes the form's elements.
            });
            promise.done(function (data) {
                $dropShipAddressContent.html(data);
                var sStatus = $("#saveDropShipAddressForm #addressId.create_update_address_id").data('status');
                if (sStatus && "success" === sStatus.toLowerCase()) {
                    TOYO.confirm.show({
                        'message': 'NOTE: You are requesting to send your order to a new address that is not one of your approved Ship To addresses. After you submit your order, a Toyo Tires Customer Service Representative will review it and approve the Drop Ship address before it can be released.',
                        'buttons': {
                            'ACCEPT': {
                                'class': 'blue',
                                'action': function (e) {
                                    ACC.deliveryaddressToyo.reloadCartPage();
                                }
                            }
                        }
                    });
                }
            }).fail(function (xht, textStatus, ex) {
                alert("Failed to update drop ship address!. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
            }).always(function () {
            });
        });
        $("#addDropShipAddressForm select").on("click", function (e) {
            e.stopPropagation();
        })
    },

    reloadCartPage: function () {
        location.reload();
    },

    bindWarehouseSelector: function() {
        var $warehouseListItems = $("#warehouseSelector .warehouse-list-item");
        $warehouseListItems.on("click", function(){
            var addressId = $(this).find(".address-id-value").val();
            var url = $("#warehouseSelector .btn-group").data('change-ship-to-url');
            ACC.deliveryaddressToyo.setShipToAddress(url, addressId);
        })

    }

}

$(document).ready(function () {
    with (ACC.deliveryaddressToyo) {
        if ($("body").hasClass("page-multiStepCheckoutSummaryPage")) {
            bindChangingAddressSelectionView();
            bindSelectionFromList();
            bindSaveDropShipAddressButton();
        } else if ($("body").hasClass("page-search") || $("body").hasClass("page-searchAdvancedEmpty")
            || $("body").hasClass("page-productList") || $("body").hasClass("page-productDetails")) {
            bindWarehouseSelector();
            bindSelectionFromList();
        }
    }
});
