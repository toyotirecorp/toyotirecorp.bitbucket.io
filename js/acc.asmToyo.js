ACC.asmToyo = {
    _autoload: [
        "refreshASMSection",
    ],

    refreshASMSection: function () {
        $('#customerColorsFragment').css('display', 'none');
        var button360 = $('.ASM-btn-customer360');
        $(button360).parent();

        $('.js-customer360').on('click', function(){
            $(document).ajaxComplete(function(){
                $('#customerColorsFragment').css('display', 'none');
                $('#customer-360-tabs li').each(function (i) {
                   var value = $(this).attr('value');
                   if (value ==='customerProfileSection' || value ==='activitySection'
                       || value ==='activitySection' || value ==='feedbackSection' || value ==='storeLocationsSection'){
                       $(this).css('display', 'none');
                   }
                });

                $('.asm__header__order').parent().hide();
                $('.asm__header__ticket').parent().hide();
                $('.asm__customer360-overview-saved-card-info').hide();
                $('.asm__customer360-overview-saved-total').hide();
                $('.asm__customer360-overview-qty-divider').hide();

            });
        });
    }
}

$(document).ready(function () {

    with (ACC.asmToyo) {
        if ($('#_asm')){
            refreshASMSection();
        }
    }
});
