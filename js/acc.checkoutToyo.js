ACC.checkoutToyo = {
    _autoload: [
        "refreshSections",
        "bindDeliverySelectionOptions",
        "refreshOptionLabel",
        "bindCheckoutButton",
        "validateRequiredFields",
        "highlightRequiredFields",
        "highlightPoNumberFields",
        "highlightWillCallFields",
        "fedexInfoChosen"

    ],

    cartData: "",

    refresh: function (data) {
        if (data == undefined) {
            $.postJSON($("#reviewContentPanel").data("checkoutCartUrl"), function (data) {
                ACC.checkoutToyo.refreshSections(data);
            });
        }
        else {
            ACC.checkoutToyo.refreshSections(data);
        }
    },

    refreshSections: function (data) {
        ACC.checkoutToyo.cartData = data;
//        TOYO.orderoptions.selectWillCallOption();
    },

    bindDeliverySelectionOptions: function () {

        function refreshOptions() {
            $(".js-delivery-option").attr("checked", false);
        }

        $("#fedex-option").on("click", function () {
            $("#ltl-info").hide();
            $("#fedex-info").show();
            ACC.checkoutToyo.refreshOptionLabel(this);
        });

        $("#ltl-option").on("click", function () {
            $("#ltl-info").show();
            $("#fedex-info").hide();
            ACC.checkoutToyo.refreshOptionLabel(this);
            ACC.checkoutToyo.refreshSubmitOrderButtonState();
        });

        $(".order-form-checkout").delegate(".js-delivery-option", "change", function () {
            var currentState = $(this).is(':checked');
            $('.carriers input').prop('checked', false);
            $(this).prop('checked', currentState);
            /*if ($('#js-total-weight').html() < 2000) {*/
                if ($('.carriers input:checked').length == 0) {
                    var postData = {
                        name: $(this).data('name'),
                        value: currentState
                    };
                } else {
                    var postData = {
                        price: $(this).data('price'),
                        name: $(this).data('name'),
                        value: $(this).val()
                    };
                }
                $.postJSON($("#shipping-info").data('change-delivery-url'), postData, function (data) {

                });
            /*}*/
        });
    },

    refreshOptionLabel: function (chosenOption) {
        var chosenOptionName = $(chosenOption).text().trim();
        $(chosenOption).parents(".btn-group").find(".toyo-dropdown-input").val(chosenOptionName);
    },


    bindCheckoutButton: function () {
        $('.checkoutButton').click(function (e) {
            e.preventDefault();

            if (ACC.checkoutToyo.validateRequiredFields()) {
                TOYO.overlay.show({});
                $(this).hide();
                $('#order-submit-loading').show();

                var deliveryAddressId = $("#deliveryAddressId").val();
                $("#placeOrderDeliveryAddressId").val(deliveryAddressId);

                var billToAddressId = $("#billToAddressId").val();
                $("#placeOrderBillToAddressId").val(billToAddressId);

                var checkoutUrl = $(this).data("checkoutUrl");
                var confirmationUrl = $(this).data("confirmationUrl");

                var placeOrderForm = $("#placeOrderForm");

                var promise = $.ajax({
                    type: "POST",
                    url: checkoutUrl,
                    data: placeOrderForm.serialize() // serializes the form's elements.
                });
                promise.done(function (data) {
                    window.location = confirmationUrl + "/" + data['code'];
                }).fail(function () {
                    $(this).show();
                    $('#order-submit-loading').hide();
                }).always(function () {
                    TOYO.overlay.hide();
                });
            }
        });
        $("#submitOrderButton").removeAttr("disabled");
    },

    validateRequiredFields: function () {
        var conditionsFulfilled = ACC.checkoutToyo.checkConditions();
        if (!conditionsFulfilled) {
            ACC.checkoutToyo.highlightRequiredFields();
        }
        return conditionsFulfilled;
    },

    highlightRequiredFields: function () {
        ACC.checkoutToyo.highlightPoNumberFields($("#poNumberInput").val() == "");
        ACC.checkoutToyo.highlightWillCallFields($("#willCallCheckbox").is(':checked') && $("#driver-name-input").text() == "");
        ACC.checkoutToyo.highlightTermsAndConditionsFields($("#termsAndConditionsCheckbox").is(':checked'));
        var shippingMethodProvided = ACC.checkoutToyo.checkShippingInformation();
        ACC.checkoutToyo.highlightShippingMethodFields(shippingMethodProvided);
    },

    highlightPoNumberFields: function (empty) {
        if (empty) {
            $("#poNumberInput").addClass("visible").addClass("highlighted");
            $("label[for=poNumberInput]").addClass("visible").addClass("highlighted");
            $("#poNumberRequiredLabel").addClass("visible");

        } else {
            $("#poNumberInput").removeClass("visible").removeClass("highlighted");
            $("label[for=poNumberInput]").removeClass("visible").removeClass("highlighted");
            $("#poNumberRequiredLabel").removeClass("visible");
        }
    },

    highlightWillCallFields: function (empty) {
        if (empty) {
            $("#driver-name-input").addClass("highlighted");
            $("#willCallRequiredLabel").addClass("visible").addClass("highlighted");
        } else {
            $("#driver-name-input").removeClass("highlighted");
            $("#willCallRequiredLabel").removeClass("visible").removeClass("highlighted");
        }
    },

    highlightTermsAndConditionsFields: function (checked) {
        if (checked) {
            $("#termsAndConditionsRequiredLabel").removeClass("visible").removeClass("highlighted");
        } else {
            $("#termsAndConditionsRequiredLabel").addClass("visible").addClass("highlighted");
        }
    },

    highlightShippingMethodFields: function (shippingMethodProvided) {
        if (shippingMethodProvided) {
            $("#carriersRequiredLabel").removeClass("visible").removeClass("highlighted");
        } else {
            $("#carriersRequiredLabel").addClass("visible").addClass("highlighted");
        }
    },

    refreshSubmitOrderButtonState: function () {
        var conditionsFulfilled = ACC.checkoutToyo.checkConditions();
    },

    checkConditions: function () {
        var result = $("#termsAndConditionsCheckbox").prop("checked");
        if (result) {
            var willCallCheckboxChecked = $("#willCallCheckbox").prop("checked");
            if (willCallCheckboxChecked) {
                var result = $("#driver-name-input").val().trim() != "";
            } else {
                result = ACC.checkoutToyo.checkShippingInformation();
            }
        }

        var $poNumberInputVal = $("#poNumberInput").val();
        var poNumberProvided =  $poNumberInputVal != null && $poNumberInputVal.trim() != "";
        return poNumberProvided && result;
    },

    checkShippingInformation: function () {
        var result = true;
        var skipShippingCheck = $('#shipping-info').css("display") == "none";
        // console.log("checkShippingInformation skipShippingCheck:" + skipShippingCheck);
        if (!skipShippingCheck) {
            result = false;
            var fedexInfoChosenResult = ACC.checkoutToyo.fedexInfoChosen();
            var ltlCheckboxChosenResult = ACC.checkoutToyo.ltlCheckboxChosen();
            // console.log("checkShippingInformation fedexInfoChosenResult:" + fedexInfoChosenResult);
            // console.log("checkShippingInformation ltlCheckboxChosenResult:" + ltlCheckboxChosenResult);
            result = fedexInfoChosenResult || ltlCheckboxChosenResult;
        }
        return result;
    },

    fedexInfoChosen: function () {
        var result = false;
        var fedexInfoDisabled = $('#fedex-info').css("display") == "none";
        if (!fedexInfoDisabled) {
            var anyRadioChecked = $('.fedex-option input:radio:checked');
            if (anyRadioChecked.length > 0) {
                result = true;
            }
        }
        return result;
    },

    ltlCheckboxChosen: function () {
        var result = false;
        var ltlInfoDisabled = $('#ltl-info').css("display") == "none";
        if (!ltlInfoDisabled) {
            var checked = $('#ltl-info input:checkbox:checked');
            if (checked.length > 0) {
                result = true;
            }
        }
        return result;
    },

    bindRetrySubmitButton: function () {
        var $retrySubmitButton = $("#retrySubmit");
        $retrySubmitButton.on("click", function(e){
            e.preventDefault();
            var url = $retrySubmitButton.data('retry-submit-url');
            var orderCode = $retrySubmitButton.data('order-number');
            var promise = $.ajax({
                url:         url,
                type:        'POST',
                data: {"orderCode": orderCode}
            });
            promise.done(function(response){
                var redirectUrl = $retrySubmitButton.data('orderConfirmationUrl');
                redirectUrl=redirectUrl+"/"+response;
                $(location).attr('href', redirectUrl);
            }).fail( function(jqXHR, textStatus, errorThrown){
                console.log("The following error occured: " + textStatus, errorThrown);
            }).always(
            )
        })

    }

}


$(document).ready(function () {

    with (ACC.checkoutToyo) {
//        refresh();
        bindDeliverySelectionOptions();
        bindCheckoutButton();
    }

    if ($("body").hasClass("page-orderConfirmationPage")) {
        with (ACC.checkoutToyo) {
            bindRetrySubmitButton();
        }
    }

});
