
ACC.productListToyo = {
    _autoload: [
        "bindCalculation",
    ],

    bindCalculation: function () {
        $("input.variant-quantity").on("change", function () {
            var valueToSet = parseInt(this.value);
            var discontinued = $(this).siblings('input[name="discontinued"]').val();
            if(discontinued === "true") {
                var stockLevelQuantity = ACC.productListToyo.getStockLevelQuantity(this);
                if(parseInt(this.value) >= stockLevelQuantity){
                    valueToSet = stockLevelQuantity;
                }
            }
            this.value = valueToSet;
            ACC.productListToyo.calculateProductTableSums();
            var bulkSearch = $(this).data("bulkSearch");
            if (bulkSearch) {
                var productCode = $(this).parents("td").first().siblings(".product-code").text()
                ACC.productListToyo.saveFormQuantity($(this), productCode, valueToSet);
            }
        });
    },

    getStockLevelQuantity: function (input) {
        var stock = parseInt($(input).parents("td:first").siblings(".quantity-avail").find(".variant-quantity-available").text());
        if (isNaN(stock)) {
            stock = parseInt($(input).parents("td:first").siblings(".quantity-avail").find(".variant-quantity-available").val());
        }
        return stock;
    },

    calculateProductTableSums: function () {
        var baseProductRows = $(".base-product-row");
        var totalQuantity = 0;
        var totalWeight = 0;
        if (baseProductRows) {
            $.each(baseProductRows, function() {
                var baseProductQuantity = 0;
                var baseProductQuantityAv = 0;
                var baseProductBackorderQuantity = 0;
                var baseProductShippedQuantity = 0;
                var baseProductWeight = 0;
                var variantRows = $(this).next(".variants-row").find(".variant-row");
                $.each(variantRows, function() {
                    var variantQuantity = ACC.productListToyo.getVariantQuantity(this);
                    var variantQuantityAvailable = ACC.productListToyo.getVariantQuantityAvailable(this);
                    var variantWeight = 0;
                     if ($("body").hasClass("page-cartPage")) {
                        var calculatedVariantBackorderQuantity = 0;
                        if (variantQuantity > variantQuantityAvailable) {
                            calculatedVariantBackorderQuantity =  variantQuantity - variantQuantityAvailable;
                        }
                        $(this).find("span.variant-backorder-quantity").text(calculatedVariantBackorderQuantity);
                        variantWeight = ACC.productListToyo.getVariantWeight(this, variantQuantity, calculatedVariantBackorderQuantity);
                        baseProductBackorderQuantity+=calculatedVariantBackorderQuantity;
                     } else {
                        var variantBackorderQuantity = ACC.productListToyo.getVariantBackorderQuantity(this);
                        variantWeight = ACC.productListToyo.getVariantWeight(this, variantQuantity, variantBackorderQuantity);
                        baseProductBackorderQuantity+=variantBackorderQuantity;

                        var variantShippedQuantity = ACC.productListToyo.getVariantShippedQuantity(this);
                        baseProductShippedQuantity+=variantShippedQuantity;

                     }

                    baseProductQuantity+=variantQuantity;
                    baseProductQuantityAv+=variantQuantityAvailable;
                    baseProductWeight+=variantWeight;
                });
                $(this).find("span.js-base-product-qty").text(baseProductQuantity);
                $(this).find("span.js-base-product-av-qty").text(baseProductQuantityAv);
                $(this).find("span.js-base-product-backorder-qty").text(baseProductBackorderQuantity);
                $(this).find("span.js-base-product-qty-shipped").text(baseProductShippedQuantity);
                $(this).find(".productTotalWeight").text(baseProductWeight.toFixed(0));
                totalQuantity+=baseProductQuantity;
                totalWeight+=baseProductWeight;
            });
        }
        $(".product-table-total-weight").text(totalWeight.toFixed(0));
        $(".product-table-total-quantity").text(totalQuantity);

        ACC.productListToyo.changeStateOfAddToOrderBtn(totalQuantity);
        return totalWeight;
    },

    getVariantQuantity: function (variantRow) {
        var variantQuantity = parseFloat($(variantRow).find(".variant-quantity").val());
        if (isNaN(variantQuantity)) {
            // take value from text instead of from input (read only mode)
            variantQuantity = parseFloat($(variantRow).find(".quantity-col.quantity").text());
        }
        if (variantQuantity < 0) {
            variantQuantity = 0;
        }
        return variantQuantity;
    },

    getVariantQuantityAvailable: function (variantRow) {
        var variantQuantityAvailable = parseFloat($(variantRow).find(".variant-quantity-available").text());
        //if (isNaN(variantQuantityAvailable)) {
            // take value from text instead of from input (read only mode)
            //variantQuantity = parseFloat($(variantRow).find(".variant-quantity").text());
        //}
        return variantQuantityAvailable;
    },

    getVariantBackorderQuantity: function (variantRow) {
        var variantBackorderQuantity = parseFloat($(variantRow).find(".variant-backorder-quantity").text());
        if (isNaN(variantBackorderQuantity)) {
            variantBackorderQuantity = 0;
        }
        return variantBackorderQuantity;
    },

    getVariantShippedQuantity: function (variantRow) {
        var variantShippedQuantity = parseFloat($(variantRow).find(".variant-quantity-shipped").text());
        if (isNaN(variantShippedQuantity)) {
            variantShippedQuantity = 0;
        }
        return variantShippedQuantity;
    },

    getVariantWeight: function (variantRow, variantQuantity, boqty) {
        var singleVariantWeight = parseFloat($(variantRow).find(".variant-weight").val());
        var variantWeight = singleVariantWeight * (variantQuantity - boqty);
        if (isNaN(variantWeight)) {
            variantWeight = 0;
        }
        return variantWeight;
    },

    changeStateOfAddToOrderBtn: function (quantity) {
        var $button = $('#addToCartBtn');
        var isPurchase = $("#purchaseStatus").val();
        if (quantity >= 1 && isPurchase == "true"){
            $button.removeAttr('disabled');
            $('#orderFormAddToCart').removeClass('disabled');
        } else {
            $button.attr('disabled','disabled');
            $('#orderFormAddToCart').addClass('disabled');
        }
    },

    handleDiscontinuedFlag: function() {
        if ($("body").hasClass("page-cartPage")) {
            var $discontinuedRows = $('input[name="discontinued"][value="true"]');
            $discontinuedRows.each(function() {
                $(this).parents("td:first").prevAll(".quantity.quantity-avail").addClass('discontinued');
            });
        } else {
            var $discontinuedRows = $('input[name="discontinued"][value="true"]');
            $discontinuedRows.each(function() {
                $(this).parent().prevAll(".quantity-avail:first").addClass('discontinued');
            });
        }
       $('.discontinued .stock-level').prepend('<span class="discontinued-message">This item has been discontinued. You may order up to the Quantity Available shown.</span>')
    },
    saveFormQuantity: function (input, productCode, quantity) {
        var updateUrl = input.data("updateUrl");
        var promise = $.ajax({
            type: "POST",
            url: updateUrl,
            data: {"productCode": productCode, "quantity" : quantity}
        });
        promise.done(function (data) {
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log("The following error occurred: " + textStatus + "->" + errorThrown);
        }).always(function () {
        });
    },

    setWidthToBasicTable : function(){
        if ($('.variants-table')){
            var ths = $($('.variants-table')[0]).find('th');
            if (ths){
                var sumLeng = 0;
                for (var i = 0; i < 6; i++){
                    sumLeng = sumLeng + Math.abs($(ths[i]).width());
                }

              var thsbasic = $('#base-product-row-1 td')[0];
                $(thsbasic).width = sumLeng;
            }
        }
    }
}

$(document).ready(function () {

    with (ACC.productListToyo) {
       if ($("body").hasClass("page-cartPage")
                   || $("body").hasClass("page-searchAdvancedEmpty")
                   || $("body").hasClass("page-order")) {
                   ACC.productListToyo.calculateProductTableSums();
                   ACC.productListToyo.bindCalculation();
               }
       if ($("body").hasClass("page-productList") || $("body").hasClass("page-search")) {
           ACC.productListToyo.bindCalculation();
       }
       ACC.productListToyo.handleDiscontinuedFlag();
       ACC.productListToyo.setWidthToBasicTable();
    }
});
