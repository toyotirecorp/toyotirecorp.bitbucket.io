ACC.productDetailToyo = {


    initPageEvents: function () {

        $('#accessibletabsnavigation0-1').on("click", function(){
            $('#filterOptions').prop('disabled', true);
        });

        $('#accessibletabsnavigation0-0').on("click", function(){
            $('#filterOptions').prop('disabled', false);
        });

        $('#accessibletabsnavigation0-2').on("click", function(){
            $('#filterOptions').prop('disabled', true);
        });

        $("#accordion").accordion({
            heightStyle: "content"
        });

        $("#filterOptions").click(function () {
            var sectionWidth = parseFloat($("#sectionWidthDropdown").val().trim());
            var aspectRatio =  parseFloat($("#aspectRatioDropdown").val().trim());
            var wheelDiameter =  parseFloat($("#wheelDiameterDropdown").val().trim());
            $.each($("#availableOptionsTab tr"), function (index, value) {
                if (
                    (parseFloat($(value).data("section-width")) === sectionWidth || !$.isNumeric(sectionWidth)) &&
                    (parseFloat($(value).data("aspect-ratio")) === aspectRatio || !$.isNumeric(aspectRatio)) &&
                    (parseFloat($(value).data("wheel-diameter")) === wheelDiameter || !$.isNumeric(wheelDiameter))
                    ) {
                    $(value).show();
                } else {
                    $(value).hide();
                }
                $("tr.header").show();
            });
            ACC.productDetailToyo.refreshTableLook(sectionWidth || aspectRatio || wheelDiameter);
            ACC.productDetailToyo.refreshFilter(sectionWidth, aspectRatio, wheelDiameter);
        });

        $(document).on("click", "#imageLink, .productImageZoomLink", function (e) {
            e.preventDefault();

            $.colorbox({
                href: $(this).attr("href"),
                height: 555,
                onComplete: function () {
                    ACC.common.refreshScreenReaderBuffer();

                    $('#colorbox .productImageGallery .jcarousel-skin').jcarousel({
                        vertical: true
                    });

                },
                onClosed: function () {
                    ACC.common.refreshScreenReaderBuffer();
                }
            });
        });


        $(".productImageGallery img").click(function (e) {
            $(".productImagePrimary img").attr("src", $(this).attr("data-primaryimagesrc"));
            $("#zoomLink, #imageLink").attr("href", $("#zoomLink").attr("data-href") + "?galleryPosition=" + $(this).attr("data-galleryposition"));
            $(".productImageGallery .thumb").removeClass("active");
            $(this).parent(".thumb").addClass("active");
        });


        $(document).on("click", "#colorbox .productImageGallery img", function (e) {
            $("#colorbox  .productImagePrimary img").attr("src", $(this).attr("data-zoomurl"));
            $("#colorbox .productImageGallery .thumb").removeClass("active");
            $(this).parent(".thumb").addClass("active");
        });


        $("body").on("keyup", "input[name=qtyInput]", function (event) {
            var input = $(event.target);
            var value = input.val();
            var qty_css = 'input[name=qty]';
            while (input.parent()[0] != document) {
                input = input.parent();
                if (input.find(qty_css).length > 0) {
                    input.find(qty_css).val(value);
                    return;
                }
            }
        });


        $("#Size").change(function () {
            var url = "";
            var selectedIndex = 0;
            $("#Size option:selected").each(function () {
                url = $(this).attr('value');
                selectedIndex = $(this).attr("index");
            });
            if (selectedIndex != 0) {
                window.location.href = url;
            }
        });

        $("#variant").change(function () {
            var url = "";
            var selectedIndex = 0;

            $("#variant option:selected").each(function () {
                url = $(this).attr('value');
                selectedIndex = $(this).attr("index");
            });
            if (selectedIndex != 0) {
                window.location.href = url;
            }
        });


        $(".selectPriority").change(function () {
            var url = "";
            var selectedIndex = 0;

            url = $(this).attr('value');
            selectedIndex = $(this).attr("index");

            if (selectedIndex != 0) {
                window.location.href = url;
            }
        });

    },

    refreshTableLook: function(filterApplied){
        $('section.table tbody tr').removeClass("filtered");
        $('section.table tbody tr').removeClass("filtered-even");
        if (filterApplied){
            $('section.table tbody tr').removeClass("filtered-even");
            $('section.table tbody tr:visible').addClass("filtered");
            $('section.table tbody tr:visible.filtered:odd').addClass("filtered-even");
        }
    },

    refreshFilter: function () {
        ACC.productDetailToyo.filerDropdownValues("section-width", "sectionWidthDropdown");
        ACC.productDetailToyo.filerDropdownValues("aspect-ratio", "aspectRatioDropdown");
        ACC.productDetailToyo.filerDropdownValues("wheel-diameter", "wheelDiameterDropdown");
    },

    filerDropdownValues: function (dataAttributeName, dropDownId) {

        function getAttributeValuesInResult(dataAttributeName) {
            var $attributeValuesInResult = $("#availableOptionsTab .filtered");
            var valueArray = $.makeArray();
            if ($attributeValuesInResult.length > 0) {
                $attributeValuesInResult.each(function () {
                    var value = parseFloat($(this).data(dataAttributeName));
                    valueArray.push(value)
                })
                valueArray = $.unique(valueArray);
            }
            return valueArray;
        }

        var resultValues = getAttributeValuesInResult(dataAttributeName);
        $dropDownValues = $("#" + dropDownId + " .dropdown-menu .list-item");
        $dropDownValues.removeClass("filtered-out");
        if (resultValues.length > 0) {
            if ($dropDownValues.length > 0) {
                $dropDownValues.each(function () {
                    $this = $(this);
                    var ddValue = parseFloat($this.html().trim());
                    if (isNaN(ddValue) || $.inArray(ddValue, resultValues) > -1) {
                    } else {
                        $this.addClass("filtered-out");
                    }
                });
            }
        }
    },

    bindFilterComponent: function () {
        var $component = $(".filter .toyo-dropdown-component");
        $component.on("listItemClick", function (e) {
            // here you can execute additional actions onClick
        });

        // bind unavailable dropdown value click
        $(".filter .toyo-dropdown-component").delegate(".list-item", "click", function () {
            $this = $(this);
            if ($this.hasClass("filtered-out")) {
                thisId = $this.parents(".toyo-dropdown-component").attr("id");
                clearOtherFilter("sectionWidthDropdown", thisId);
                clearOtherFilter("aspectRatioDropdown", thisId);
                clearOtherFilter("wheelDiameterDropdown", thisId);
            }

            function clearOtherFilter(dropDownId, thisId) {
                if (thisId === dropDownId) {
                    /*alert("same, do nothig")*/
                } else {
                    /*alert("clear "+dropDownId)*/
                    $("#" + dropDownId + " .toyo-dropdown-input").val("");
                }
            }
        });
    },
    loadProductSizeAvailable: function(){
        if ($('.product-list-table')){
            $('#availableOptionsTab').DataTable( {
                paging: false,
                responsive: true,
                ordering: true,
                 columnDefs: [
                    { responsivePriority: 1, targets: 0},
                    { responsivePriority: 2, targets: -1 },
                    { "targets": [11], "searchable": false, "orderable": false, "visible": true },
                    { "targets": [10], "searchable": false, "orderable": false, "visible": true },
                    { "targets": [9], "searchable": false, "orderable": true, "visible": true }
                 ],
                  "bInfo" : false,
            } );

            $('.spec-table').DataTable( {
                     paging: false,
                     responsive: true,
                     searching: false,
                     ordering: false,
                      columnDefs: [
                         { responsivePriority: 1, targets: 0},
                         { responsivePriority: 2, targets: -1 },
                      ],
                       "bInfo" : false,
                 } );
        }
    }
};

$(document).ready(function () {

    with (ACC.productDetailToyo) {
        initPageEvents();
        bindFilterComponent();
        loadProductSizeAvailable();
    }
});

