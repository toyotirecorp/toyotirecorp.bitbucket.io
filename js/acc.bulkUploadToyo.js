ACC.bulkUploadToyo = {
    _autoload: [
    ],

    bindAll: function () {
        $(".file-drop").click(function(){
            $("#fileupload").click();
        });

        function resetBulkUpload(){
            $("#create-order-btn").hide();
            $("#upload-success-text").hide();
            var $bar = $('#progress .bar');
            $bar.css('width','0%');
            $("#upload-fail-text").hide();
            $("#upload-progress").hide();
            $(".download-sample").hide();
        }

        $('#fileupload').fileupload({
            dataType: 'json',
            enctype: 'multipart/form-data',
            done: function (e, response) {
                if(response.result.loaded === true) {
                    resetBulkUpload();
                    $("#create-order-btn").show();
                    $("#upload-success-text").show();
                    $("#upload-progress").hide();
                    ACC.bulkUploadToyo.showTemplate(
                        {template: $("#bulk-validation-result").tmpl({data: response.result})}
                    );
                } else {
                    resetBulkUpload();
                    $("#upload-progress").hide();
                    $("#upload-fail-text").show();
                    $(".download-sample").show();
                }

            },

            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var $bar = $('#progress .bar');
                $bar.css('width',progress + '%');
            },

            start: function (e) {
                resetBulkUpload();
                $("#upload-def-text").hide();
                $("#upload-progress").show();
            },

            fail: function (e) {
                resetBulkUpload();
                $("#upload-progress").hide();
                $("#upload-fail-text").show();
                $(".download-sample").show();
            },

            dropZone: $('#dropzone'),
            pasteZone: null
        });

        var obj = $("#dropzone");
        obj.on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).addClass("upl-area-solid").removeClass("upl-area-dashed");
        });
        obj.on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });
        $(document).on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });
        $(document).on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            obj.addClass("upl-area-dashed").removeClass("upl-area-solid");
        });
    },
    show: function(params){
        if (params){
            if($('#confirmOverlay').length){
                        // A confirm is already shown on the page:
                        return false;
                    }

                    var buttonHTML = '';
                    $.each(params.buttons,function(name,obj){

                        // Generating the markup for the buttons:

                        buttonHTML += '<a href="#" class="button btn-toyo '+obj['class']+'">'+name+'</a>';

                        if(!obj.action){
                            obj.action = function(){};
                        }
                    });

                    var markup = [
                        '<div id="confirmOverlay">',
                        '<div id="confirmBox">',
                        '<p>',params.message,'</p>',
                        '<div id="confirmButtons">',
                        buttonHTML,
                        '</div></div></div>'
                    ].join('');

                    $(markup).hide().appendTo('body').fadeIn();

                    var buttons = $('#confirmBox .button'),
                        i = 0;

                    $.each(params.buttons,function(name,obj){
                        buttons.eq(i++).click(function(){

                            // Calling the action attribute when a
                            // click occurs, and hiding the confirm.
                            obj.action(params.context);
                            acc.confirmToyo.hide();
                            return false;
                        });
                    });
        }

    },

        showTemplate: function(params){
            if (params){
                if($('#confirmOverlay').length){
                    // A confirm is already shown on the page:
                    return false;
                }

                var markup = [
                    '<div id="validOverlay">',
                    '<div id="validBox">',
                    $('<div/>').append(params.template).html(),
                    '</div></div>'
                ].join('');

                $(markup).hide().appendTo('body').fadeIn();
                var popupTitle = "File Uploaded ...";
                ACC.colorbox.open(popupTitle,{
                    inline: true,
                    height: false,
                    href: $(markup),
                    onComplete: function ()
                    {

                        $(this).colorbox.resize();
                    }
                });

            }
        },

        hide: function() {
            $('#confirmOverlay').fadeOut(function () {
                $(this).remove();
            });
        }
}

$(document).ready(function () {

    with (ACC.bulkUploadToyo) {
        ACC.bulkUploadToyo.bindAll();
//        ACC.bulkUploadToyo.showTemplate();
//        ACC.bulkUploadToyo.show();
//        ACC.bulkUploadToyo.hide();
    }
});
