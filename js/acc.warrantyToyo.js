ACC.warrantyToyo = {
  refreshSubmitOrderButtonState: function () {
    var $termsAndConditionsCheckbox = $("#termsAndConditionsCheckbox");
    var checked = $termsAndConditionsCheckbox.is(":checked");
    var $submitClaim = $("#submitClaim");

    if (checked) {
      $submitClaim.removeClass("disabled");
    } else {
      $submitClaim.addClass("disabled");
    }
  },

  bindTermsAndConditionCheck: function () {
    $("#termsAndConditionsCheckbox").on("click", function () {
      ACC.warrantyToyo.refreshSubmitOrderButtonState();
    });
  },

  bindClaimType: function (e) {
    $(document).on("click", "#claimType", function (e) {
      ACC.warrantyToyo.displayClaimType(e);
    });
  },

  displayClaimType: function (e) {
    var claimType = $("#claimType:checked").val();

    if (claimType === "noregrets") {
      $(".no-regrets-wrapper").show();
      $(".mileage-wrapper").hide();
    } else {
      $(".mileage-wrapper").show();
      $(".no-regrets-wrapper").hide();
    }
  },
  bindReason: function () {
    $(document).on("change", "#reason", function (e) {
      ACC.warrantyToyo.displayOterReason(e);
    });
  },

  displayOterReason: function (e) {
    var reason = $("#reason").val();
    if (reason === "Other") {
      $("#otherreason").removeAttr("disabled");
    } else {
      $("#otherreason").attr("disabled", "disabled");
    }
  },
};

$(document).ready(function () {
  with (ACC.warrantyToyo) {
    bindClaimType();
    bindReason();
    bindTermsAndConditionCheck();
  }
});
