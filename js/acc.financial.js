ACC.financial = {

    _autoload: [
        "bindReportSelector",
        "bindInvoiceFilter",
        "bindInvoicePageSize",
        "bindDealerSelectChange"
    ],

    validate: function (dateInput) {
        var stringDateValue = dateInput.val();
        if (stringDateValue != ""){
            try {
                jQuery.datepicker.parseDate("mm/dd/yy",  stringDateValue);
                return true;
            }
            catch(e) {
                // show modal?
                return false;
            }
        } else {
            return true;
        }
    },

    validateInputLength: function($el, inputLength, $errorEl) {
        var $val = $el.val();
        if ($val.length > 0 && $val.length < inputLength) {
            $el.addClass('error');
            $errorEl.addClass('error');
            ACC.financial.bindRevalidation($el);
            return false;
        } else {
            return true;
        }
    },

    validateDealerId: function($el, $errorEl) {
        var $val = $el.val();
        if ($val.length > 0) {
            $el.addClass('error');
            $errorEl.addClass('error');
            ACC.financial.bindRevalidation($el);
            return false;
        } else {
            return true;
        }
    },

    bindDealerSelectChange: function() {
        var $dealerSelect = $("#dealer-select");
        $dealerSelect.on("change", function () {
            var dealerId = $dealerSelect.val();
            console.log("selected: " + dealerId);
            ACC.financial.doInvoiceSearchSort(dealerId);
        })
    },

    bindRevalidation: function ($input) {
        var $toyoInputs = $('.toyo-inputs');
        $input.on('keyup.searchInput', function() {
            if($(this).val().length === 0 || $(this).val().length > 3) {
                $(this).removeClass('error');
                $input.off('searchInput');
                if (!($toyoInputs.hasClass('error'))) {
                    $('.toyo-input-error-msg').removeClass('error');
                }

            }
        });
    },

    bindReportSelector: function () {
        $(document).ready(function () {
            if ($("#reports-selector-wrapper")) {

                $('.stats-all-selector').click(function (event) {
                    event.stopImmediatePropagation();
                });

                $('.stat-header').click(function () {
                    $(this).toggleClass('open');
                    $(this).next('.stat-body').slideToggle('fast');
                });
            }
        });
    },

    bindInvoiceFilter: function () {
        var $filterSubmitButton = $("#submitInvoiceFiter");
        $filterSubmitButton.on("click", function () {
            ACC.financial.doInvoiceSearchSort();
        });
    },

    bindInvoicePageSize: function () {
        var $invoicePageSizeSelect = $("select.search-result-view-select");
        $invoicePageSizeSelect.on("change", function () {
            var $pgSizeInput = $("#page-size-input");
            $pgSizeInput.val(this.value);
            ACC.financial.doInvoicePageSize();
        })
    },

    doInvoicePageSize: function () {
        var currentUrl = window.location.href;
        var rootUrl = window.location.href.split('?')[0];
        var params = $.url('?', currentUrl);

        var $toyoInput = $(".toyo-input");
        var dealerId = $("#dealer-select").val();
        var dateFromInputValue = $("#dateFromInput").val();
        var dateToInputValue = $("#dateToInput").val();
        var $invoiceNumberInput = $("#invoiceNumberInput");
        var invoiceNumberInputValue = $invoiceNumberInput.val();
        var $poNumberInput = $("#invoicePoNumberInput");
        var poNumberInputValue = $poNumberInput.val();
        var $pgSizeInput = $("#page-size-input");
        var pgSizeValue = $pgSizeInput.val();
        var $toyoInputError = $(".toyo-input-error-msg");
        $toyoInput.removeClass('error');
        $toyoInputError.removeClass('error');
        if (ACC.financial.validateInputLength($pgSizeInput, 2, $toyoInputError)) {
            var updatedUrl = rootUrl;
            var sortValue = $.url('?sort', currentUrl);
            var sortOrder = $.url('?sortOrder', currentUrl);
            params = $.param({
                dealerId: dealerId,
                dateFrom: dateFromInputValue,
                dateTo: dateToInputValue,
                page:0,
                sortOrder: sortOrder,
                sort:sortValue,
                pgsize:pgSizeValue,
                invoiceNumber: invoiceNumberInputValue,
                poNumber: poNumberInputValue
            });
            updatedUrl = updatedUrl +"?" + params;

            if (updatedUrl != currentUrl) {
                window.location = updatedUrl;
            }
        }
    },

    doInvoiceSearchSort: function (dealerId) {
        var currentUrl = window.location.href;
        var rootUrl = window.location.href.split('?')[0];
        var params = $.url('?', currentUrl);

        var $toyoInput = $(".toyo-input");
        if (dealerId == undefined) {
            dealerId = $("#dealer-select").val();
        }
        var $dealerId = $("#dealer-select");
        var dateFromInputValue = $("#dateFromInput").val();
        var dateToInputValue = $("#dateToInput").val();
        var $invoiceNumberInput = $("#invoiceNumberInput");
        var invoiceNumberInputValue = $invoiceNumberInput.val();
        var $poNumberInput = $("#invoicePoNumberInput");
        var poNumberInputValue = $poNumberInput.val();
        var $pgSizeInput = $("#page-size-input");
        var pgSizeValue = $pgSizeInput.val();
        var $invoiceTypeInput = $("#invoiceTypeInput");
		var invoiceTypeInputValue = $invoiceTypeInput.val();
		var $invoiceShipToDealerInput = $("#invoiceShipToDealerInput");
 		var invoiceShipToDealerInputValue = $invoiceShipToDealerInput.val();

        var $toyoInputError = $(".toyo-input-error-msg");
        $toyoInput.removeClass('error');
        $toyoInputError.removeClass('error');
        if ((ACC.financial.validate($("#dateFromInput")) && ACC.financial.validate( $("#dateToInput"))
            && ACC.financial.validateInputLength($invoiceNumberInput, 4, $toyoInputError) && ACC.financial.validateInputLength($poNumberInput, 4, $toyoInputError))
            || ACC.financial.validateDealerId($dealerId, $toyoInputError)) {

            var currentDateFromParamValue = $.url('?dateFrom', currentUrl);
            var currentDateToParamValue = $.url('?dateTo', currentUrl);
            var currentInvoiceNumberParamValue = $.url('?invoiceNumber', currentUrl);
            var currentPoNumberParamValue = $.url('?poNumber', currentUrl);
            var currentInvoiceTypeInputValue = $.url('?invoiceType', currentUrl);
            var currentInvoiceShipToDealerInputValue = $.url('?shipToDealer', currentUrl);
           
            var updatedUrl = rootUrl;
            if (dateFromInputValue != "" || typeof currentDateFromParamValue != "undefined" ||
                dateToInputValue != "" || typeof currentDateToParamValue != "undefined" ||
                invoiceNumberInputValue != "" || typeof currentInvoiceNumberParamValue != "undefined" ||
                poNumberInputValue != "" || typeof currentPoNumberParamValue != "undefined" ||
                invoiceTypeInputValue != "" || typeof currentInvoiceTypeInputValue != "undefined" ||
                invoiceShipToDealerInputValue != "" || typeof currentInvoiceShipToDealerInputValue != "undefined" ||
                $dealerId != "" || typeof $dealerId != "undefined")  {
                var sortValue = $.url('?sort', currentUrl);
                var pgSizeValue = $.url('?pgsize', currentUrl);
                var sortOrder = $.url('?sortOrder', currentUrl);
                params = $.param({
                    dealerId: dealerId,
                    dateFrom: dateFromInputValue,
                    dateTo: dateToInputValue,
                    page:0,
                    sortOrder: sortOrder,
                    sort:sortValue,
                    pgsize:pgSizeValue,
                    invoiceNumber: invoiceNumberInputValue,
                    poNumber: poNumberInputValue,
                    invoiceType : invoiceTypeInputValue,
                    shipToDealer:invoiceShipToDealerInputValue
                });
                updatedUrl = updatedUrl +"?" + params;
            }

            if (updatedUrl != currentUrl) {
                window.location = updatedUrl;
            }
        }
    }
};


$('.single-checkbox').on('change', function() {
	if($('.single-checkbox:checked').length > 0) {
		$('#downloadPdf').prop('disabled', false);
	   }
		var limit = document.getElementById("checkboxLimit").value;
			if($('.single-checkbox:checked').length >= limit) {
				$('.single-checkbox:unchecked').attr("disabled", true);
			}else {
				$('.single-checkbox:unchecked').attr("disabled", false);
		}
	});