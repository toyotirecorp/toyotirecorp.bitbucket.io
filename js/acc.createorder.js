ACC.createorder = {

    _autoload: [
        "bindViewTiresButton",
        "bindSearchSubmit",
        "bindSearchInput",
        "bindViewSwitcher"
    ],

    $expandPatternsButton: $("#expand-tread-patterns"),
    $searchSubmit:		   $(".js-search-submit"),
    $searchInput:          $(".js-query-input"),

    bindViewTiresButton: function(){
        $(".pattern-selection").delegate(".js-view-tread-pattern", "click", function(){
            var searchPatternUrl = $(this).data("search-url");
            window.location=searchPatternUrl;
        });
    },

    loadPageState: function() {
        if(sessionStorage.getItem('openHomepagePanelCode')) {
            var elementId = sessionStorage.getItem('openHomepagePanelCode');
            $("#"+elementId).addClass("open");
            $("#"+elementId+" > label.expand").addClass("open");
            if ("searchCatalogSection" === elementId) {
                $("#buildOrderSection").removeClass("open");
                $("#buildOrderSection label.expand").removeClass("open");
            }
        }
    },

    bindSearchSubmit: function () {
        var submitButton = ACC.createorder.$searchSubmit;
        submitButton.click(function (event) {
            var searchType = $(this).data('search-type');
            var searchInput = $(".js-query-input[data-search-type=" + searchType + "]");
            var searchString = searchInput.val().trim();
            event.preventDefault();
            event.stopPropagation();
            ACC.createorder.doSearch(searchType, searchString);
        });
    },

    bindSearchInput: function () {
        var searchInput = ACC.createorder.$searchInput;
        searchInput.keyup(function(event) {
            var searchType = $(this).data('search-type');
            var searchInput = $(".js-query-input[data-search-type=" + searchType + "]");
            var searchString = searchInput.val().trim();
            if (event.keyCode === 13) {
                event.preventDefault();
                ACC.createorder.doSearch(searchType, searchString);
            }
        });
    },

    doSearch: function(searchType, searchString) {
        if (searchString) {
            var valueEnteredRegexp = new RegExp('[a-zA-Z0-9]');
            if (valueEnteredRegexp.test(searchString)) {
                var find = ' ';
                var re = new RegExp(find, 'g');
                if (searchType === "catalog") {
                    window.location = ACC.config.springUrl + "search/?text=" + searchString;
                }
                else if (searchType === "form") {
                    window.location = ACC.config.springUrl + "search/product-advanced?keywords=" + searchString.replace(re, '') + "&searchResultType=order-form&onlyProductIds=true&_onlyProductIds=on&skus=&isCreateOrderForm=false";
                }
            }
        }
    },

    bindViewSwitcher: function(){
        var view = sessionStorage.getItem('view');
        var isList = !view || view === null || view === 'list';
        ACC.createorder.showHideList(!isList);
       $('.search-result-view-icon').on('click',function(e) {
           $('.search-result-view-icon').removeClass("search-result-view-icon-active");
           sessionStorage.removeItem('view');
           var isGrid = $(this).hasClass('grid');
           ACC.createorder.showHideList(isGrid);
       });
    },
    showHideList: function(isGrid){
       if (isGrid) {
           sessionStorage.setItem('view', 'grid');
           $('.search-result-list').hide();
           $('.search-result-grid').css("display", "block");
           $('.search-result-grid .product__grid').css("display", "block");
           $('.search-result-view-icon.grid').addClass("search-result-view-icon-active");
       } else{
           sessionStorage.setItem('view', 'list');
           $('.search-result-grid').hide();
           $('.search-result-list').show();
           $('.search-result-list').css("display", "table");
           $('.search-result-view-icon.list').addClass("search-result-view-icon-active");
           $('.search-result-list .glyphicon-shopping-cart').addClass('pull-right');
       }
    },

    bindCreateBulkOrderBtn: function(button) {
        button.click(function() {
            window.location = ACC.config.springUrl+"search/product-advanced?keywords=&searchResultType=order-form&onlyProductIds=true&_onlyProductIds=on&skus=&isCreateOrderForm=false&isBulkSearch=true"
        });
    },


};

$(document).ready(function () {
    ACC.createorder.bindViewTiresButton();
    ACC.createorder.loadPageState();
    ACC.createorder.bindViewSwitcher();
    ACC.createorder.bindCreateBulkOrderBtn($("#create-bulk-order"));
});


