ACC.invoice = 
{
		_autoload: [
			"applySortCriteria",
			"invokeSortOnInvoiceTableHeaderClick"
		],
		applySortCriteria : function()
		{
			var invoiceTable = $("#sales-rep-sort");
			var criteria = (typeof invoiceTable.data("sort-criteria") != "undefined" && 
								invoiceTable.data("sort-criteria") !== "") ? invoiceTable.data("sort-criteria").split("|") : "";
			if ("" !== criteria)
			{
				var column = criteria[0];
				column = column.substring(2, column.length).toLowerCase();
				var order = criteria[1];
				
				if (column !== "" && order !== "")
				{
					var thArr = invoiceTable.find("th");
					var thC = '';
					$.each(thArr, function(index, val)
					{
						thC = typeof $(val).attr("class") != 'undefined' ? $(val).attr("class").toLowerCase() : "";
						if (thC.indexOf(column) !== -1)
						{
							$(val).removeClass("sort").addClass("sort_"+order);
							return false;
						}
					});
				}
			}
		},
		invokeSortOnInvoiceTableHeaderClick : function()
		{
			$("#sales-rep-sort tr th").click(function()
			{
				var link = $(this).find("a");
				if (typeof link != "undefined" && typeof link.attr("href") != "undefined")
				{
					window.location = link.attr("href");
				}
			});
		}
};
