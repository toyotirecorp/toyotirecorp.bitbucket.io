
ACC.drivenToyo = {
    _autoload: [
        "bindDrivenLoad",
    ],

    bindDrivenLoad: function () {
        $('.punchout-panel a[href="/en/driven/start-earning"]').on('click', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            if (url.indexOf('driven/start-earning') < 0){
                return true;
            }
            $.ajax({
                url: url,
                type: "get",
                success: function (response){
                    if (response){
                        var result = $.parseJSON(response);
                        var siteCustomer = result.customerId.split('#');
                        var url = result.drivenUrl + "?customerId=" + siteCustomer[1] + "&baseSiteId=" + siteCustomer[0] + "&drivenId=";
                        var loyaltyList = result.loyaltyList[0];
                        if (loyaltyList.length > 1){
                            var output = [];
                            $.each(loyaltyList, function(key, value)
                            {
                              var loyaltyUrl = url + value;
                              output.push('<a href="' + loyaltyUrl + '" target="_blank" class="loyalty-link"> <li>' + value + '</li></a> </br>');
                            });

                            var boxContent = $(output.join(' '));
                            ACC.colorbox.open('Please choose Loyalty Id',{
                                href: boxContent,
                                inline: true,
                                width:"525px",
                                closeButton: true,
                                onComplete: function ()
                                {
                                    $(this).colorbox.resize();
                                    $('.loyalty-link').on('click', function(e){
                                        $(this).colorbox.close();
                                    })
                                }
                            });

                        }else if (loyaltyList.length === 1){
                            var loyaltyUrl = url + loyaltyList[0];
                            window.open(loyaltyUrl, '_blank');
                            return false;
                        }
                    }
                }
            });
            return false;
        });
    }
}

$(document).ready(function () {

    with (ACC.drivenToyo) {
       bindDrivenLoad();
    }
});
