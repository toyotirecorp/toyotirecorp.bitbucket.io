ACC.searchToyo = {
    _autoload: [
        "bindAll",
    ],


    bindAll: function() {
        with (ACC.searchToyo) {
            bindSearchSubmit($searchSubmit);
            bindTopSearchSubmit($topMenuSearchSubmit, $topMenuSearchInput);
            bindCreateBulkOrderBtn($("#create-bulk-order"));
            bindCancelProductFromUploadBtn();
        }
    },

    bindTopSearchSubmit: function(submitButton, searchInput){
        submitButton.click(function (event) {
            var searchString = searchInput.val();
            if (!searchString) {
                event.preventDefault();
                event.stopPropagation();
            }
        });
    },

    bindSearchSubmit: function (submitButton) {
        submitButton.click(function (event) {

            event.preventDefault();
            event.stopPropagation();

            var searchType = $(this).data('search-type');
            var searchInput = $(".js-query-input[data-search-type=" + searchType + "]");
            var searchString = searchInput.val().trim();
            if (searchString) {
                var valueEnteredRegexp = new RegExp('[a-zA-Z0-9]');
                if (valueEnteredRegexp.test(searchString)) {
                    var find = ' ';
                    var re = new RegExp(find, 'g');
                    if (searchType === "catalog") {
                        window.location = ACC.config.springUrl + "search/?text=" + searchString;
                    }
                    else if (searchType === "form") {
                        window.location = ACC.config.springUrl + "search/product-advanced?keywords=" + searchString.replace(re, '') + "&searchResultType=order-form&onlyProductIds=true&_onlyProductIds=on&skus=&isCreateOrderForm=false";
                    }
                }
            }
        });
    },

    bindCreateBulkOrderBtn: function(button) {
        button.click(function() {
            window.location = ACC.config.springUrl+"search/product-advanced?keywords=&searchResultType=order-form&onlyProductIds=true&_onlyProductIds=on&skus=&isCreateOrderForm=false&isBulkSearch=true"
        });
    },

    bindCancelProductFromUploadBtn: function() {
        $(".advance-search-results .add_to_cart_order_form").on("click", ".submitRemoveOrderEntry", function(e) {
            e.preventDefault();
            $(e.target).closest(".variant-row").find(".quantity-col .variant-quantity").val(0).change().focusout();
        });
    }
}

$(document).ready(function () {

    ACC.searchToyo.bindAll

});
